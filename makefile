all:
	make -C ntu/csie/oop13spring all
	javac *.java
run:
	java ntu.csie.oop13spring.POOFight Court Lawyer Prosecutor
clean:
	make -C ntu/csie/oop13spring clean
	rm -f *.class