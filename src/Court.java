import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import ntu.csie.oop13spring.*;
import java.awt.image.BufferedImage;
import java.util.Random;

public class Court extends POOArena implements MouseListener, ActionListener{
	JFrame frm;
	component[] comp = new component[100];
	int comp_index;
	final int MAP_SIZE=10;
	Random rand_num; 
	private JLabel[][] map;
	boolean[][] dead = new boolean[MAP_SIZE][MAP_SIZE]; //store which witnesses are done.
	int[][] imap = new int[MAP_SIZE][MAP_SIZE]; //store things on map
	JLabel status_frame;
	boolean first_time_fight;
	POOPet[] parr;
	boolean[] Mdead;	//0:Lawyer dead, 1:Prosecutor dead
	Lawyer L;
	Prosecutor P;
	JLabel Lgraph;
	JLabel Pgraph;
	JLabel lefttext;
	JLabel righttext;
	JLabel midgraph;
	JLabel Mtext;
	JLabel Btext;
	StringBuffer bottext;
	StringBuffer midtext;
	JPanel JP;
	boolean CorB;			//Convince(True) or Buy out(False)
	private int game_round;
	private int game_state; //0: Lawyer's turn		1: Lawyer moving		2:Lawyer after moving		3: Lawyer attack/bribe
							//4: Prosecutor's turn	5: Prosecutor moving    6:Prosecutor after moving	7: Prosecutor attack/bribe
	private int final_judge;
	public Court(){
		int i,j;
		//initialize
		Mdead = new boolean[2];
		Mdead[0] = false;
		Mdead[1] = false;
		for(i=0;i<MAP_SIZE;i++)
			for(j=0;j<MAP_SIZE;j++)
				dead[i][j] = false;
		CorB = false;
		game_round = 0;
		final_judge = 0;
		first_time_fight = true;
		rand_num = new Random();
		comp_index = 0;
		frm = new JFrame();
		frm.setSize(580,770);
		frm.setResizable(false);
		frm.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frm.setTitle("Ace Attorney");
		//Lawyer's graph
		Lgraph = new JLabel();
		frm.add(Lgraph);
		//text in the left-middle
		lefttext = new JLabel("<html>= Lawyer =<br/>HP: 100<br/>Persuasion: 50<br/>Money: 0<br/>Agility: 2</html>"); 
		lefttext.setFont(new Font("Serif", Font.PLAIN, 13));
		frm.add(lefttext);
		//judge graph in the middle
		midgraph = new JLabel();
		midtext = new StringBuffer("");
		Mtext = new JLabel(midtext.toString());
		Mtext.setFont(new Font(Font.DIALOG, Font.BOLD, 14));
		midgraph.setIcon(new ImageIcon("graph/judge(0).png"));
		JP = new JPanel();
		JP.setLayout(new GridLayout(2,1));
		JP.add(midgraph);
		JP.add(Mtext);
		frm.add(JP);
		//text in the right-middle
		String rtext = null;
		righttext = new JLabel("<html>= Prosecutor =<br/>HP: 100<br/>Persuasion: 10<br/>Money: 100<br/>Agility: 4</html>");
		righttext.setFont(new Font("Serif", Font.PLAIN, 13));
		frm.add(righttext);
		//Prosecutor's graph
		Pgraph = new JLabel();
		frm.add(Pgraph);
		//create things on map
		map = new JLabel[MAP_SIZE][MAP_SIZE];
		this.set_map();
		for(i=0;i<MAP_SIZE;i++){
			map[i] = new JLabel[MAP_SIZE];
			for(j=0;j<MAP_SIZE;j++){
				map[i][j] = new JLabel();
				map[i][j].addMouseListener(this);
				frm.add(map[i][j]);
			}
		}
		Btext = new JLabel();
		bottext = new StringBuffer("Game start.");
		Btext.setText(bottext.toString());
		Btext.setFont(new Font(Font.DIALOG, Font.BOLD, 13));
		frm.add(Btext);
		//print all things on map
		System.out.println("Set map done");
		frm.setLayout(new FlowLayout()/*GridLayout(MAP_SIZE+1, MAP_SIZE)*/);
		frm.setVisible(true);
		comp_index = 0;
	}
	public boolean fight(){
		if(first_time_fight == true){
			first_time_fight = false;
			parr = super.getAllPets();
			L = (Lawyer)parr[0];
			P = (Prosecutor)parr[1];
			System.out.println("first_time_fight Done.");
			return true;
		}
		if(game_round == 10){
			if(final_judge <= 0)
				show_guilty();
			else
				show_innocent();
			return false;
		}
		if(Mdead[0] == true){
			show_guilty();
			return false;
		}
		if(Mdead[1] == true){
			show_innocent();
			return false;
		}
		return true;
	}
	public void show(){
		int i,j;
		//System.out.println("In show.");
		String[] path = new String[2];
		path[0] = L.get_Big_path(final_judge);
		path[1] = P.get_Big_path(final_judge);
		Lgraph.setIcon(new ImageIcon(path[0]));
		Pgraph.setIcon(new ImageIcon(path[1]));
		midtext = new StringBuffer("<html>Round "+game_round+"<br/>Judge's view: ");
		if(final_judge <= 0)
			midtext.append("guilty.<br/>Influence point = "+final_judge+".");
		else
			midtext.append("innocent.<br/>Influence point = "+final_judge+".");
		StringBuffer Ltext = new StringBuffer("<html>= Lawyer =<br/>HP: ");
		StringBuffer Rtext = new StringBuffer("<html>= Prosecutor =<br/>HP: ");
		Ltext.append(L.get_HP()+"<br/>Persuasion: "+L.persuasion+"<br/>Money: "+L.money+"<br/>Agility: "+L.get_AGI()+"</html>");
		Rtext.append(P.get_HP()+"<br/>Persuasion: "+P.persuasion+"<br/>Money: "+P.money+"<br/>Agility: "+P.get_AGI()+"</html>");
		lefttext.setText(Ltext.toString());
		righttext.setText(Rtext.toString());
		Mtext.setText(midtext.toString());
		for(i=0;i<MAP_SIZE;i++){
			for(j=0;j<MAP_SIZE;j++){
				String Path = get_node_name(i,j);
				ImageIcon Ix = new ImageIcon(Path);
				ImageIcon Iy = new ImageIcon();
				if(dead[i][j] == true){
					if(comp[imap[i][j]].belongsto == 5566)
						Iy.setImage(this.change_color(Ix.getImage(), 0));
					else if(comp[imap[i][j]].belongsto == -5566)
						Iy.setImage(this.change_color(Ix.getImage(), 1));
				}
				else if(this.check_color_change(i,j))
					Iy.setImage(this.change_color(Ix.getImage(), 2));
				else
					Iy.setImage(Ix.getImage());
				map[i][j].setIcon(Iy);
			}
		}
		Btext.setText(bottext.toString());
		//System.out.println("Show done.");
	}
	private void show_guilty(){
		String final_text = "<html>After "+game_round+" days, the Lawyer can't provide enough strength of evidence <br/>and witness to prove that the accused is innocent.<html>";
		String final_text2 = "<html>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;So the final judge is...&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<html/>";
		JLabel Ftext = new JLabel(final_text);
		JLabel Ftext2 = new JLabel(final_text2);
		Ftext.setFont(new Font(Font.SANS_SERIF, Font.PLAIN, 14));
		Ftext2.setFont(new Font(Font.SANS_SERIF, Font.BOLD, 18));
		Ftext.setHorizontalTextPosition(JLabel.CENTER);
		JDialog JD = new JDialog(frm, "Final Judge");
		JD.add(Lgraph);
		JLabel guilty = new JLabel(new ImageIcon("graph/judge(2).png"));
		JD.add(Pgraph);
		JD.add(Ftext);
		JD.add(Ftext2);
		JD.add(guilty);
		JD.setLayout(new FlowLayout());
		JD.setBounds(40,10,500,500);
		JD.setVisible(true);
	}
	private void show_innocent(){
		String final_text = "<html>After "+game_round+" days, the Lawyer strongly provided some evidences <br/>and witness to show that the accused is absolute innocent.<html>";
		String final_text2 = "<html>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;So the final judge is...&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<html/>";
		JLabel Ftext = new JLabel(final_text);
		JLabel Ftext2 = new JLabel(final_text2);
		Ftext.setFont(new Font(Font.SANS_SERIF, Font.PLAIN, 14));
		Ftext2.setFont(new Font(Font.SANS_SERIF, Font.BOLD, 18));
		Ftext.setHorizontalTextPosition(JLabel.CENTER);
		JDialog JD = new JDialog(frm, "Final Judge");
		JD.add(Lgraph);
		JLabel guilty = new JLabel(new ImageIcon("graph/judge(3).png"));
		JD.add(Pgraph);
		JD.add(Ftext);
		JD.add(Ftext2);
		JD.add(guilty);
		JD.setLayout(new FlowLayout());
		JD.setBounds(40,10,500,500);
		JD.setVisible(true);
	}
	private void set_map(){
		int i,j,x;
		int num,attack,mistrust,influence;
		int pb=0,mb=0,ab=0,hb=0;
		boolean[] wit = new boolean[30];
		for(i=0;i<30;i++)
			wit[i] = false;
		for(i=0;i<MAP_SIZE;i++){
			for(j=0;j<MAP_SIZE;j++){
				if((i==(MAP_SIZE-1)) && (j==0)){ //lawyer here
					imap[i][j] = 5566;
					continue;
				}
				else if((i==0) && (j==(MAP_SIZE-1))){ //prosecutor here
					imap[i][j] = -5566;
					continue;
				}
				x = rand_num.nextInt(5);
				if(x == 0){ //witness
					num = rand_num.nextInt(30);
					while(wit[num] == true){
						num++;
						if(num == 30)
							num = 0;
						if(rand_num.nextInt(50) == 0)
							break;
					}
					wit[num] = true;
					attack = rand_num.nextInt(100);
					mistrust = 100 - attack + rand_num.nextInt(50);
					influence = mistrust + attack - rand_num.nextInt(70);
					comp[comp_index] = new component(attack, mistrust, influence, num);
					imap[i][j] = comp_index;
					comp_index++;
				}
				else if(x == 1){ //evidence
					num = rand_num.nextInt(10);
					pb = rand_num.nextInt(3)*10*rand_num.nextInt(2);
					if(pb == 0)
						mb = (rand_num.nextInt(5)+3)*10;
					ab = (rand_num.nextInt(2)+1)*rand_num.nextInt(2);
					if(ab == 0)
						hb = (rand_num.nextInt(3)+2)*10;
					comp[comp_index] = new component(pb, mb, ab, hb, num);
					imap[i][j] = comp_index;
					comp_index++;
				}
				else{ //blank
					imap[i][j] = -1;
				}
			}
		}
	}
	private boolean check_color_change(int I, int J){
		int i=0,j=0;
		int pi=0,pj=0;	//position of Lawyer or Prosecutor
		if(game_state == 1){
			for(i=0;i<MAP_SIZE;i++)
				for(j=0;j<MAP_SIZE;j++)
					if(imap[i][j] == 5566){
						pi = i;
						pj = j;
					}
			if(dist(I,J,pi,pj) <= L.get_AGI())
				return true;
		}
		if(game_state == 3){
			for(i=0;i<MAP_SIZE;i++)
				for(j=0;j<MAP_SIZE;j++)
					if(imap[i][j] == 5566){
						pi = i;
						pj = j;
					}
			if(dist(I,J,pi,pj) == 1)
				return true;
		}
		else if(game_state == 5){
			for(i=0;i<MAP_SIZE;i++)
				for(j=0;j<MAP_SIZE;j++)
					if(imap[i][j] == -5566){
						pi = i;
						pj = j;
					}
			if(dist(I,J,pi,pj) <= P.get_AGI())
				return true;
		}
		else if(game_state == 7){
			for(i=0;i<MAP_SIZE;i++)
				for(j=0;j<MAP_SIZE;j++)
					if(imap[i][j] == -5566){
						pi = i;
						pj = j;
					}
			if(dist(I,J,pi,pj) == 1)
				return true;
		}
		return false;
	}
	private int dist(int i, int j, int pi, int pj){
		int dist=0;
		if(i > pi)
			dist += i-pi;
		else
			dist += pi-i;
		if(j > pj)
			dist += j-pj;
		else
			dist += pj-j;
		return dist;
	}
	private Image change_color(Image image, int type){
		BufferedImage BI = new BufferedImage(image.getWidth(null), image.getHeight(null), BufferedImage.TYPE_INT_RGB);
		BI.getGraphics().drawImage(image, 0, 0 , null);
		int w = BI.getWidth();
		int h = BI.getHeight();
		int i,j;
		int r,g,b;
		int RGB;
		for(i=0;i<w;i++){
			for(j=0;j<h;j++){
				RGB = BI.getRGB(i,j);
				if(type == 0){
					if(check_gray(RGB))
						RGB = 255*256*256;
					else
						RGB = RGB*-1;
				}
				else if(type == 1){
					if(check_gray(RGB))
						RGB = 255;
					else
						RGB = RGB*-1;
				}
				else if(type == 2){
					if(check_gray(RGB) && ((game_state == 1) || (game_state == 5)))
						RGB = (120 + 160*256 + 120*256*256);
					else if(check_gray(RGB) && ((game_state == 3) || (game_state == 7)))
						RGB = (160 + 120*256 + 120*256*256);
				}
				BI.setRGB(i,j,RGB);
			}
		}
		return BI;
	}
	private boolean check_gray(int RGB){
		int R = ((RGB & 0xFF0000) >> 16);
		int G = ((RGB & 0xFF00) >> 8);
		int B = (RGB & 0xFF);
		if(R<=170 && R>=100)
			if(G<=170 && G>= 100)
				if(B<=170 && B>= 100)
					if((R-G)<=15 && (R-G)>=-15)
						if((G-B)<=15 && (G-B)>=-15)
							if((R-B)<=15 && (R-B)>=-15)
								return true;
		return false;
	}
	private String get_node_name(int i, int j){
		if(imap[i][j] == -1)
			return "graph/blank.png";
		else if(imap[i][j] == 5566){	//lawyer
			if(game_state < 4)
				return L.get_path();
			else
				return L.get_path2();
		}
		else if(imap[i][j] == -5566){	//prosecutor
			if(game_state > 3)
				return P.get_path();
			else
				return P.get_path2();
		}
		return comp[imap[i][j]].get_path();
	}
    public void mouseClicked(MouseEvent e) {
		int i,j;
		int I=-1;
		int J=-1;
		for(i=0;i<MAP_SIZE;i++)
			for(j=0;j<MAP_SIZE;j++)
				if(e.getSource().equals(map[i][j])){
					I = i; 
					J = j;
				}
		System.out.println("Mouse clicked on graph ("+I+","+J+").");
		if(game_state == 0)
			when_game_state_0_2_4_6(I,J,1);
		else if(game_state == 1)
			when_game_state_1(I,J);
		else if(game_state == 2)
			when_game_state_0_2_4_6(I,J,1);
		else if(game_state == 3)
			when_game_state_3(I,J);
		else if(game_state == 4)
			when_game_state_0_2_4_6(I,J,-1);
		else if(game_state == 5)
			when_game_state_5(I,J);
		else if(game_state == 6)
			when_game_state_0_2_4_6(I,J,-1);
		else if(game_state == 7)
			when_game_state_7(I,J);
    }
	public void when_game_state_3(int I, int J){
		int i=0,j=0;
		int pi=0,pj=0;
		//find where Lawyer is
		for(i=0;i<MAP_SIZE;i++)
			for(j=0;j<MAP_SIZE;j++)
				if(imap[i][j] == 5566){
					pi = i;
					pj = j;
				}
		if((imap[I][J] >= 100) || (imap[I][J] < 0)){
			bottext = new StringBuffer("Invalid action!");
			System.out.println("Invalid action!");
			game_state = 2;
		}
		else if((dist(I,J,pi,pj)==1) && (comp[imap[I][J]].witness == true) && (comp[imap[I][J]].belongsto == 0)){
			if(CorB == true){	//Convince
				int tru = ((int)(L.persuasion*0.4) + rand_num.nextInt(10));
				int att = ((int)(comp[imap[I][J]].attack*0.5) + rand_num.nextInt(10));
				System.out.println("Lawyer try to convince the witness, lowering witness' mistrust by "+tru+".");
				System.out.println("Witness attack the Lawyer! Lawyer's HP decrease by "+att+".");
				bottext = new StringBuffer("Witness' mistrust decrease by "+tru+", Lawyer's HP decrease by "+att+"!");
				comp[imap[I][J]].mistrust -= tru;
				int hp = L.get_HP();
				if(hp > att)
					L.set_HP((hp-att));
				else{
					System.out.println("Lawyer is dead!");
					bottext = new StringBuffer("Lawyer is dead!");
					Mdead[0] = true;
				}
				if(comp[imap[I][J]].mistrust <= 0){
					System.out.println("The witness finally convinced by the Lawyer. The judge is influenced by "+comp[imap[I][J]].influence+" point.");
					bottext = new StringBuffer("The witness convinced by lawyer. Influence point +"+comp[imap[I][J]].influence);
					comp[imap[I][J]].belongsto = 5566;
					comp[imap[I][J]].attack = 0;
					comp[imap[I][J]].mistrust = 0;
					final_judge += comp[imap[I][J]].influence;
					comp[imap[I][J]].influence = 0;
					dead[I][J] = true;
				}
				game_state = 4;
			}
			else{	//Buy out
				int mist = comp[imap[I][J]].mistrust;
				int mon = L.money;
				if(mon >= (mist*2)){
					System.out.println("Lawyer spends "+(mist*2)+" dollars to buy out the witness");
					System.out.println("Witness' mistrust low down by "+mist+".");
					System.out.println("The witness is buying out by the Lawyer. The judge is influenced by "+comp[imap[I][J]].influence+" point.");
					bottext = new StringBuffer("The witness is buying out by lawyer. Influence point +"+comp[imap[I][J]].influence);
					comp[imap[I][J]].belongsto = 5566;
					comp[imap[I][J]].attack = 0;
					comp[imap[I][J]].mistrust = 0;
					final_judge += comp[imap[I][J]].influence;
					comp[imap[I][J]].influence = 0;
					dead[I][J] = true;
					L.money = (mon-(mist*2));
				}
				else{
					System.out.println("Lawyer spends "+mon+" dollars to buy out the witness");
					System.out.println("Witness' mistrust low down by "+(mon/2)+".");
					bottext = new StringBuffer("Lawyer spends "+mon+" dollars decrease Witness' mistrust by "+(mon/2)+".");
					comp[imap[I][J]].mistrust = (mist-(mon/2));
					L.money = 0;
				}
				game_state = 4;
			}
		}
		else{
			System.out.println("Invalid action!");
			bottext = new StringBuffer("Invalid action!");
			game_state = 2;
		}
	}
	public void when_game_state_7(int I, int J){
		int i,j;
		int pi=0,pj=0;
		//find where Lawyer is
		for(i=0;i<MAP_SIZE;i++)
			for(j=0;j<MAP_SIZE;j++)
				if(imap[i][j] == -5566){
					pi = i;
					pj = j;
				}
		if((imap[I][J] >= 100) || (imap[I][J] < 0)){
			System.out.println("Invalid action!");
			bottext = new StringBuffer("Invalid action!");
			game_state = 6;
		}
		else if((dist(I,J,pi,pj)==1) && (comp[imap[I][J]].witness == true) && (comp[imap[I][J]].belongsto == 0)){
			if(CorB == true){	//Convince
				int tru = ((int)(P.persuasion*0.4) + rand_num.nextInt(10));
				int att = ((int)(comp[imap[I][J]].attack*0.5) + rand_num.nextInt(10));
				System.out.println("Prosecutor try to convince the witness. Witness' mistrust low down by "+tru+".");
				System.out.println("Witness attack the Prosecutor! Prosecutor's HP decrease by "+att+".");
				bottext = new StringBuffer("Witness' mistrust decrease by "+tru+", Prosecutor's HP decrease by "+att+"!");
				comp[imap[I][J]].mistrust -= tru;
				int hp = P.get_HP();
				if(hp > att)
					P.set_HP((hp-att));
				else{
					System.out.println("Prosecutor is dead!");
					bottext = new StringBuffer("Prosecutor is dead!");
					Mdead[1] = true;
				}
				if(comp[imap[I][J]].mistrust <= 0){
					System.out.println("The witness finally convince by the Prosecutor. The judge is influenced by "+comp[imap[I][J]].influence+" point.");
					bottext = new StringBuffer("The witness convinced by prosecutor. Influenced point -"+comp[imap[I][J]].influence);
					comp[imap[I][J]].belongsto = -5566;
					comp[imap[I][J]].mistrust = 0;
					comp[imap[I][J]].attack = 0;
					final_judge -= comp[imap[I][J]].influence;
					comp[imap[I][J]].influence = 0;
					dead[I][J] = true;
				}
				game_state = 0;
				game_round += 1;
			}
			else{	//Buy out
				int mist = comp[imap[I][J]].mistrust;
				int mon = P.money;
				if(mon >= (mist*2)){
					System.out.println("Prosecutor spends "+(mist*2)+" dollars to buy out the witness");
					System.out.println("Witness' mistrust low down by "+mist+".");
					System.out.println("The witness is buying out by the Prosecutor. The judge is influenced by "+comp[imap[I][J]].influence+" point.");
					bottext = new StringBuffer("The witness is buying out by prosecutor. Influenced point +"+comp[imap[I][J]].influence);
					comp[imap[I][J]].belongsto = -5566;
					comp[imap[I][J]].mistrust = 0;
					comp[imap[I][J]].attack = 0;
					final_judge -= comp[imap[I][J]].influence;
					comp[imap[I][J]].influence = 0;
					dead[I][J] = true;
					P.money = (mon-(mist*2));
				}
				else{
					System.out.println("Prosecutor spends "+mon+" dollars to buy out the witness");
					System.out.println("Witness' mistrust low down by "+(mon/2)+".");
					bottext = new StringBuffer("Prosecutor spends "+mon+" dollars decrease Witness' mistrust by "+(mon/2)+".");
					comp[imap[I][J]].mistrust = (mist-(mon/2));
					P.money = 0;
				}
				game_state = 0;
				game_round += 1;
			}
		}
		else{
			System.out.println("Invalid action!");
			bottext = new StringBuffer("Invalid action!");
			game_state = 6;
		}
	}
	public void when_game_state_1(int I, int J){
		int i,j;
		int pi=0,pj=0;
		//find where Lawyer is
		for(i=0;i<MAP_SIZE;i++)
			for(j=0;j<MAP_SIZE;j++)
				if(imap[i][j] == 5566){
					pi = i;
					pj = j;
				}
		//check if it's inside the moving range
		if(dist(I,J,pi,pj) == 0)
			game_state = 0;
		else if((dist(I,J,pi,pj) <= L.get_AGI()) && (imap[I][J] == -1)){
			//move to position I,J
			imap[pi][pj] = -1;
			imap[I][J] = 5566;
			game_state = 2;
			if(check_surrounding(I,J))
				game_state = 4;
		}
		else if((dist(I,J,pi,pj) <= L.get_AGI()) && (comp[imap[I][J]].evidence == true)){
			//pick up the evidence on position I,J
			imap[pi][pj] = -1;
			L.persuasion += comp[imap[I][J]].persuasion_buff;
			L.money += comp[imap[I][J]].money_buff;
			L.set_AGI(comp[imap[I][J]].agi_buff + L.get_AGI());
			L.set_HP(comp[imap[I][J]].hp_buff + L.get_HP());
			System.out.println("Lawyer picks up the evidence #"+imap[I][J]+"!!");
			System.out.println("\tPersuasion increase by "+comp[imap[I][J]].persuasion_buff);
			System.out.println("\tMoney increase by "+comp[imap[I][J]].money_buff);
			System.out.println("\tAgility increase by "+comp[imap[I][J]].agi_buff);
			System.out.println("\tHealth point increase by "+comp[imap[I][J]].hp_buff);
			bottext = new StringBuffer("Lawyer picks up the evidence: Persuasion+"+comp[imap[I][J]].persuasion_buff+", Money+"+comp[imap[I][J]].money_buff+", AGI+"+comp[imap[I][J]].agi_buff+", HP+"+comp[imap[I][J]].hp_buff);
			imap[I][J] = 5566;
			game_state = 2;
			if(check_surrounding(I,J))
				game_state = 4;
		}
		else{
			System.out.println("Can't arrive here!");
			bottext = new StringBuffer("Can't arrive here!");
			game_state = 0;
		}
	}
	public void when_game_state_5(int I, int J){
		int i,j;
		int pi=0,pj=0;
		//find where Prosecutor is
		for(i=0;i<MAP_SIZE;i++)
			for(j=0;j<MAP_SIZE;j++)
				if(imap[i][j] == -5566){
					pi = i;
					pj = j;
				}
		//check if it's inside the moving range
		if(dist(I,J,pi,pj) == 0)
			game_state = 4;
		else if((dist(I,J,pi,pj) <= P.get_AGI()) && (imap[I][J] == -1)){
			//move to position I,J
			imap[pi][pj] = -1;
			imap[I][J] = -5566;
			game_state = 6;
			if(check_surrounding(I,J)){
				game_state = 0;
				game_round += 1;
			}
		}
		else if((dist(I,J,pi,pj) <= P.get_AGI()) && (comp[imap[I][J]].evidence == true)){
			//pick up the evidence on position I,J
			imap[pi][pj] = -1;
			P.persuasion += comp[imap[I][J]].persuasion_buff;
			P.money += comp[imap[I][J]].money_buff;
			P.set_AGI(comp[imap[I][J]].agi_buff + P.get_AGI());
			P.set_HP(comp[imap[I][J]].hp_buff + P.get_HP());
			System.out.println("Prosecutor get some buff from evidence #"+imap[I][J]+"!!");
			System.out.println("\tPersuasion increase by "+comp[imap[I][J]].persuasion_buff);
			System.out.println("\tMoney increase by "+comp[imap[I][J]].money_buff);
			System.out.println("\tAgility increase by "+comp[imap[I][J]].agi_buff);
			System.out.println("\tHealth point increase by "+comp[imap[I][J]].hp_buff);
			bottext = new StringBuffer("Prosecutor gets some buff from evidence: Persuasion+"+comp[imap[I][J]].persuasion_buff+", Money+"+comp[imap[I][J]].money_buff+", AGI+"+comp[imap[I][J]].agi_buff+", HP+"+comp[imap[I][J]].hp_buff);
			imap[I][J] = -5566;
			game_state = 6;
			if(check_surrounding(I,J)){
				game_state = 0;
				game_round += 1;
			}
		}
		else{
			System.out.println("Can't arrive here!");
			bottext = new StringBuffer("Can't arrive here!");
			game_state = 4;
		}
	}
	public boolean check_surrounding(int I, int J){
		//if exists no witness surround after moving, then skip this round
		if(I > 0)
			if((imap[I-1][J] >= 0) && (imap[I-1][J] <= 100))
				if(comp[imap[I-1][J]].witness == true)
					if(dead[I-1][J] == false)
						return false;
		if(I < (MAP_SIZE-1))
			if((imap[I+1][J] >= 0) && (imap[I+1][J] <= 100))
				if(comp[imap[I+1][J]].witness == true)
					if(dead[I+1][J] == false)
						return false;
		if(J > 0)
			if((imap[I][J-1] >= 0) && (imap[I][J-1] <= 100))
				if(comp[imap[I][J-1]].witness == true)
					if(dead[I][J-1] == false)
						return false;
		if(J < (MAP_SIZE-1))
			if((imap[I][J+1] >= 0) && (imap[I][J+1] <= 100))
				if(comp[imap[I][J+1]].witness == true)
					if(dead[I][J+1] == false)
						return false;
		return true;
	}		
	public void when_game_state_0_2_4_6(int I, int J, int sep){
		if(imap[I][J] == (-5566*sep)){
			JMenuItem men1 = new JMenuItem("It's not his round!");
			JPopupMenu poMenu = new JPopupMenu(); 
			poMenu.add(men1); 
			poMenu.show(frm,(map[I][J].getX()+40),(map[I][J].getY()+30));
		}	
		else if(imap[I][J] == (5566*sep))				//click on Lawyer or Prosecutor!
			show_main_character_action(I,J);
	}
	private void show_main_character_action(int I, int J){
		if((game_state == 0) || (game_state == 4)){
			int i;
			JMenuItem[] select = new JMenuItem[4];
			select[0] = new JMenuItem("Move");
			select[1] = new JMenuItem("Convince");
			select[2] = new JMenuItem("Bribe");
			select[3] = new JMenuItem("Rest");
			JPopupMenu Menu = new JPopupMenu();
			for(i=0;i<4;i++){
				select[i].addActionListener(this);
				Menu.add(select[i]);
			}
			Menu.show(frm,(map[I][J].getX()+40),(map[I][J].getY()+30));	
		}
		if((game_state == 2) || (game_state == 6)){
			int i;
			JMenuItem[] select = new JMenuItem[3];
			select[0] = new JMenuItem("Convince");
			select[1] = new JMenuItem("Bribe");
			select[2] = new JMenuItem("Rest");
			JPopupMenu Menu = new JPopupMenu();
			for(i=0;i<3;i++){
				select[i].addActionListener(this);
				Menu.add(select[i]);
			}
			Menu.show(frm,(map[I][J].getX()+40),(map[I][J].getY()+30));	
		}
	}
	public void actionPerformed(ActionEvent e) {//active when click menuitems
		if((game_state == 0) || (game_state == 2)){
			System.out.println("Selected: " + e.getActionCommand());
			String cmd = e.getActionCommand();
			if(cmd == "Convince"){
				game_state = 3;
				CorB = true;
			}
			else if(cmd == "Bribe"){
				game_state = 3;
				CorB = false;
			}
			else if(cmd == "Move")
				game_state = 1;
			else if(cmd == "Rest")
				game_state = 4;
			else
				System.out.println("ActionPerform unknown!");
			System.out.println("Game state: "+game_state);
		}
		else if((game_state == 4) || (game_state == 6)){
			System.out.println("Selected: " + e.getActionCommand());
			String cmd = e.getActionCommand();
			if(cmd == "Convince"){
				game_state = 7;
				CorB = true;
			}
			else if(cmd == "Bribe"){
				game_state = 7;
				CorB = false;
			}
			else if(cmd == "Move")
				game_state = 5;
			else if(cmd == "Rest"){
				game_state = 0;
				game_round += 1;
			}
			else
				System.out.println("ActionPerform unknown!");
			System.out.println("Game state: "+game_state);
		}
	}
	public void mousePressed(MouseEvent e) {	//just to override abstract method
		;
    }
	public void mouseReleased(MouseEvent e) {	//just to override abstract method
		;
	}
	public void mouseEntered(MouseEvent e) {
		int i,j;
		int I=-1;
		int J=-1;
		for(i=0;i<MAP_SIZE;i++)
			for(j=0;j<MAP_SIZE;j++)
				if(e.getSource().equals(map[i][j])){
					I = i; 
					J = j;
				}
		JPopupMenu poMenu = new JPopupMenu();
			if((imap[I][J]>=0) && (imap[I][J]<=100)){	//evidence or witness
				if(comp[imap[I][J]].witness == true){		//witness
					String a = "Attack: "+comp[imap[I][J]].attack;
					String b = "Mistrust: "+comp[imap[I][J]].mistrust;
					String c = "Influence: "+comp[imap[I][J]].influence;
					String d;
					if(comp[imap[I][J]].belongsto == 5566)
						d = "belongs to LAWYER";
					else if(comp[imap[I][J]].belongsto == -5566)
						d = "belongs to PROSECUTOR";
					else
						d = "belongs to Nobody";
					JMenuItem men1 = new JMenuItem(a); 
					JMenuItem men2 = new JMenuItem(b); 
					JMenuItem men3 = new JMenuItem(c);
					JMenuItem men4 = new JMenuItem(d); 
					poMenu.add(men1); 
					poMenu.add(men2); 
					poMenu.add(men3); 
					poMenu.add(men4); 
				}
				else if(comp[imap[I][J]].evidence == true){	//evidence
					String a = "Persuasion buff: "+comp[imap[I][J]].persuasion_buff;
					String b = "Money buff: "+comp[imap[I][J]].money_buff;
					String c = "Agility buff: "+comp[imap[I][J]].agi_buff;
					String d = "Health buff: "+comp[imap[I][J]].hp_buff;
					JMenuItem men1 = new JMenuItem(a); 
					JMenuItem men2 = new JMenuItem(b); 
					JMenuItem men3 = new JMenuItem(c);
					JMenuItem men4 = new JMenuItem(d);
					poMenu.add(men1); 
					poMenu.add(men2); 
					poMenu.add(men3); 
					poMenu.add(men4); 
				}
			}
		poMenu.show(frm,(map[I][J].getX()+40),(map[I][J].getY()+30));
    }
    public void mouseExited(MouseEvent e) {		//just to override abstract method
		;
    }
	public POOCoordinate getPosition(POOPet p){	//just to override abstract method
		return null;
	}
}