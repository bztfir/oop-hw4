public class component{
	boolean witness = false;
	boolean evidence = false;
	private String path = null;
	// bonus for evidence
	int persuasion_buff = 0;
	int money_buff = 0;
	int agi_buff = 0;
	int hp_buff = 0;
	// feature for witness
	int attack = 0;
	int mistrust = 0;
	int influence = 0;
	int belongsto = 0;
	public component(int pbuff, int mbuff, int abuff, int hbuff, int num){
		evidence = true;
		persuasion_buff = pbuff;
		money_buff = mbuff;
		agi_buff = abuff;
		hp_buff = hbuff;
		path = "graph/evi ("+num+").png";
	}
	public component(int att, int mist, int inf, int num){
		witness = true;
		attack = att;
		mistrust = mist;
		influence = inf;
		path = "graph/head ("+num+").png";
	}
	public String get_path(){
		String x = path;
		//System.out.println(x+"\n"+path);
		return x;
	}
}