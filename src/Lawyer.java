import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import ntu.csie.oop13spring.*;

public class Lawyer extends POOPet{
	int persuasion;
	int money;
	String path;
	String path2;
	public Lawyer(){
		persuasion = 70;
		money = 0;
		super.setHP(100);
		super.setAGI(2);
		path = "graph/lawyer(0).png";
		path2 = "graph/lawyer(8).png";
	}
	public String get_Big_path(int FJ){
		if(FJ >= 100)
			return "graph/lawyer(6).png";
		else if(FJ >= 30)
			return "graph/lawyer(2).png";
		else if(FJ <= -100)
			return "graph/lawyer(3).png";
		else if(FJ <= -30)
			return "graph/lawyer(7).png";
		else if(FJ == 0)
			return "graph/lawyer(1).png";
		else
			return "graph/lawyer(4).png";
	}
	public String get_path(){
		return path.toString();
	}
	public String get_path2(){
		return path2.toString();
	}
	public int get_AGI(){
		return super.getAGI();
	}
	public int get_HP(){
		return super.getHP();
	}
	public void set_AGI(int x){
		boolean z = super.setAGI(x);
	}
	public void set_HP(int x){
		boolean z = super.setHP(x);
	}
	public POOAction act(POOArena arena){ //not in use
		return null;
	}
	public POOCoordinate move(POOArena arena){ //not in use
		return null;
	}
}