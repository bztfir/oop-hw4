import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import ntu.csie.oop13spring.*;

public class Prosecutor extends POOPet{
	int persuasion;
	int money;
	String path;
	String path2;
	public Prosecutor(){
		persuasion = 30;
		money = 70;
		super.setHP(100);
		super.setAGI(4);
		path = "graph/sword(0).png";
		path2 = "graph/sword(5).png";
	}
	public String get_Big_path(int FJ){
		if(FJ > 70)
			return "graph/sword(4).png";
		if(FJ < -70)
			return "graph/sword(2).png";
		return "graph/sword(1).png";
	}
	public String get_path(){
		return path.toString();
	}
	public String get_path2(){
		return path2.toString();
	}
	public int get_AGI(){
		return super.getAGI();
	}
	public int get_HP(){
		return super.getHP();
	}
	public void set_AGI(int x){
		boolean z = super.setAGI(x);
	}
	public void set_HP(int x){
		boolean z = super.setHP(x);
	}
	public POOAction act(POOArena arena){ //not in use
		return null;
	}
	public POOCoordinate move(POOArena arena){ //not in use
		return null;
	}
}